package deps

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTypeDescription(t *testing.T) {
	tests := []struct {
		name    string
		depType Type
		want    string
	}{
		{"Unknown", -1, ""},
		{"Ruby", Ruby, "Ruby gems"},
		{"JavaScript", JavaScript, "JavaScript modules"},
		{"Go", Go, "Go packages"},
		{"Python Poetry", PythonPoetry, "Python packages"},
		{"Python Pip", PythonPip, "Python packages"},
		{"Python Conda", PythonConda, "Python packages"},
		{"PHP Composer", PHP, "PHP packages"},
		{"Java Maven", JavaMaven, "Java dependencies"},
		{"Java Gradle", JavaGradle, "Java dependencies"},
		{"Kotlin Gradle", KotlinGradle, "Kotlin dependencies"},
		{"C/C++ Conan (txt)", CppConanTxt, "C/C++ dependencies"},
		{"C/C++ Conan (python)", CppConanPy, "C/C++ dependencies"},
		{"C/C++ vcpkg", CppVcpkg, "C/C++ dependencies"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := TypeDescription(tt.depType)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestNewDependency(t *testing.T) {
	tests := []struct {
		name string
		pkg  string
		ver  string
		want Dependency
	}{
		{"php dependency with version", "symfony/console", "5.4.*", Dependency{Name: "symfony/console", Version: "5.4.*"}},
		{"no version", "symfony/console", "", Dependency{Name: "symfony/console"}},
		{"java dependency with version", "junit", "2.0.10", Dependency{Name: "junit", Version: "2.0.10"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewDependency(tt.pkg, tt.ver)
			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewRubyGem(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", "rails", Dependency{Name: "rails"}},
		{"valid with version", "rails (~> 7.0.8)", Dependency{Name: "rails", Version: "(~> 7.0.8)"}},
		{"valid with versions", "capybara (~> 3.39, >= 3.39.2)", Dependency{Name: "capybara", Version: "(~> 3.39, >= 3.39.2)"}},
		{"valid with version bang", "attr_encrypted (~> 3.2.4)!", Dependency{Name: "attr_encrypted", Version: "(~> 3.2.4)!"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewRubyGem(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewGoPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", "  github.com/joho/godotenv v1.5.1", Dependency{Name: "github.com/joho/godotenv", Version: "v1.5.1"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewGoPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonPoetryPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", `pytest = "^7.2.0"`, Dependency{Name: "pytest", Version: "^7.2.0"}},
		{
			"valid with nested version",
			`uvicorn = { extras = ["standard"], version = "^0.20.0"`,
			Dependency{Name: "uvicorn", Version: "^0.20.0"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonPoetryPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonPipPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"matching version", "fastapi==0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"minimum version", "fastapi>=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"excluded version", "fastapi!=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"compatible version", "fastapi~=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"matching version with spaces", "fastapi == 0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonPipPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonCondaPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"matching version", "fastapi=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"wildcard version", "fastapi=0.104.*", Dependency{Name: "fastapi", Version: "0.104.*"}},
		{"no version", "fastapi", Dependency{Name: "fastapi"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonCondaPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestParseGradleDependency(t *testing.T) {
	tests := []struct {
		name    string
		raw     string
		want    Dependency
		wantErr bool
	}{
		{"valid", `    implementation 'com.acme:example:1.0'`, NewDependency("example", "1.0"), false},
		{"valid with dash", `implementation 'com.acme:example-one:1.0'`, NewDependency("example-one", "1.0"), false},
		{"no version", `    implementation 'com.acme:example'`, Dependency{}, true},
		{
			"test dependency",
			`    testImplementation 'org.junit.jupiter:junit-jupiter:5.9.1'`,
			NewDependency("junit-jupiter", "5.9.1"),
			false,
		},
		{
			"double quotes",
			`	implementation "org.eclipse.jdt:junit:3.3.0"`,
			NewDependency("junit", "3.3.0"),
			false,
		},
		{
			"with parenthesis",
			`	implementation("org.eclipse.jdt:junit:3.3.0")`,
			NewDependency("junit", "3.3.0"),
			false,
		},
		{
			"with parenthesis and brackets",
			`	implementation("org.eclipse.jdt:junit:3.3.0") {`,
			NewDependency("junit", "3.3.0"),
			false,
		},
		{
			"kotlin string literal format",
			`	"implementation"("org.eclipse.jdt:junit:3.3.0") {`,
			NewDependency("junit", "3.3.0"),
			false,
		},
		{
			"kotlin string literal format",
			`	"testImplementation"("org.eclipse.jdt:junit:3.3.0") {`,
			NewDependency("junit", "3.3.0"),
			false,
		},
		{
			"long version",
			`implementation group: 'com.google.code.gson', name: 'gson-parent', version: '2.10.1'`,
			NewDependency("gson-parent", "2.10.1"),
			false,
		},
		{
			"long version order 1",
			`implementation name: 'gson-parent', version: '2.10.1', group: 'com.google.code.gson'`,
			NewDependency("gson-parent", "2.10.1"),
			false,
		},
		{
			"long version order 2",
			`implementation name: 'gson-parent', group: 'com.google.code.gson', version: '2.10.1'`,
			NewDependency("gson-parent", "2.10.1"),
			false,
		},
		{
			"long version order 3",
			`implementation version: '2.10.1', name: 'gson-parent', group: 'com.google.code.gson'`,
			NewDependency("gson-parent", "2.10.1"),
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseGradleDependency(tt.raw)

			if tt.wantErr {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestParseCppConanTxtDependency(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", "sqlite3/3.37.0", NewDependency("sqlite3", "3.37.0")},
		{"with a comment", "sqlite3/3.37.0 # DB dependency", NewDependency("sqlite3", "3.37.0")},
		{"with @ format", "sqlite3/3.37.0@conan/stable", NewDependency("sqlite3", "3.37.0")},
		{"no version", "sqlite3", NewDependency("sqlite3", "")},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParseCppConanTxtDependency(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestParseCppConanPyDependency(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", `"sqlite3/3.37.0"`, NewDependency("sqlite3", "3.37.0")},
		{"trailing comma", `"sqlite3/3.37.0",`, NewDependency("sqlite3", "3.37.0")},
		{"with a comment", `"sqlite3/3.37.0", # DB dependency`, NewDependency("sqlite3", "3.37.0")},
		{"with self.requires format", `  self.requires("sqlite3/3.37.0")`, NewDependency("sqlite3", "3.37.0")},
		{
			"with self.requires with @ format",
			`  self.requires("sqlite3/3.37.0@conan/stable")`,
			NewDependency("sqlite3", "3.37.0"),
		},
		{"no version", `"sqlite3"`, NewDependency("sqlite3", "")},
		{"no version trailing comma", `"sqlite3",`, NewDependency("sqlite3", "")},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParseCppConanPyDependency(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestParseCppConanPyDependencies(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want []Dependency
	}{
		{"valid", `"sqlite3/3.37.0"`, []Dependency{NewDependency("sqlite3", "3.37.0")}},
		{"with many dependencies and no spaces", `"sqlite3/3.37.0","opencv/4.6.0"`, []Dependency{NewDependency("sqlite3", "3.37.0"), NewDependency("opencv", "4.6.0")}},
		{"with many dependencies and proper spacing", `"sqlite3/3.37.0", "opencv/4.6.0"`, []Dependency{NewDependency("sqlite3", "3.37.0"), NewDependency("opencv", "4.6.0")}},
		{"with many dependencies and closing parenthesis", `"sqlite3/3.37.0", "opencv/4.6.0")`, []Dependency{NewDependency("sqlite3", "3.37.0"), NewDependency("opencv", "4.6.0")}},
		{"trailing comma", `"sqlite3/3.37.0",`, []Dependency{NewDependency("sqlite3", "3.37.0")}},
		{"with a comment", `"sqlite3/3.37.0", # DB dependency`, []Dependency{NewDependency("sqlite3", "3.37.0")}},
		{"no version", `"sqlite3"`, []Dependency{NewDependency("sqlite3", "")}},
		{"no version trailing comma", `"sqlite3",`, []Dependency{NewDependency("sqlite3", "")}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParseCppConanPyDependencies(tt.raw)

			require.ElementsMatch(t, tt.want, got)
		})
	}
}
