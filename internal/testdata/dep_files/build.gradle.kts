group = "com.acme"
dependencies {
    implementation("com.acme:example:1.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.1")
    implementation("org.eclipse.jdt:junit:3.3.0")
    testImplementation("com.example:test-support:1.3") {
        exclude(module: "junit")
    }
    "implementation"("com.acme:example-string-literal:1.0")
    "testImplementation"("com.acme:example-test-string-literal:1.0")
}