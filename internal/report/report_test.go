package report

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func TestSave(t *testing.T) {
	fileName := "test_libs.json"
	dstDir := "reports"
	dstFile := filepath.Join(dstDir, "libs", fileName)
	expected := `{"scannerVersion":"1.0.0","fileName":"Gemfile.lock","checksum":"1234567890","libs":[` +
		`{"name":"rails","version":"~> 7.1.0"},` +
		`{"name":"kaminari","version":">= 1.2.0"}]}`

	r := New(dstDir, fileName, "1.0.0", "Gemfile.lock", "1234567890")
	r.Libs = []deps.Dependency{
		{Name: "rails", Version: "~> 7.1.0"},
		{Name: "kaminari", Version: ">= 1.2.0"},
	}

	_ = r.Save()

	// Test that file is created
	if _, err := os.Stat(dstFile); os.IsNotExist(err) {
		t.Errorf("Expected file %s to exist", dstFile)
	}

	// Test file contains expected data
	content, err := os.ReadFile(dstFile)
	if err != nil {
		t.Errorf("Failed to read test file: %v", err)
	}
	if string(content) != expected {
		t.Errorf("File contains wrong data. Got %v, want %v",
			string(content), expected)
	}

	// Clean up file
	err = os.Remove(dstFile)
	if err != nil {
		t.Errorf("Failed to remove test file: %v", err)
	}
}
