package main

import (
	"encoding/json"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/discovery"
	"go.uber.org/goleak"
)

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestValidateAIClientData(t *testing.T) {
	tests := []struct {
		name     string
		apiV4Url string
		jobID    string
		token    string
		wantErr  bool
	}{
		{"valid data", "http://127.0.0.1:3000/api/v4", "1", "secret", false},
		{"url missing", "", "1", "secret", true},
		{"jobID missing", "http://127.0.0.1:3000/api/v4", "", "secret", true},
		{"token missing", "http://127.0.0.1:3000/api/v4", "1", "", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testApp := application{
				aiclientData: struct {
					apiV4Url string
					jobID    string
					token    string
				}{tt.apiV4Url, tt.jobID, tt.token},
			}

			gotErr := testApp.validateAIClientData()
			if tt.wantErr {
				require.Error(t, gotErr)
			} else {
				require.NoError(t, gotErr)
			}
		})
	}
}

const internalTestData = "../../internal/testdata/dep_files"

func TestCreateReports(t *testing.T) {
	type TestReport struct {
		FileName       string `json:"fileName"`
		ScannerVersion string `json:"scannerVersion"`
		Libs           []struct {
			Name string `json:"name"`
		} `json:"libs"`
	}

	tests := []struct {
		name       string
		scanDir    string
		depsByType map[deps.Type][]*discovery.DependencyFile
		expected   map[string]string
	}{
		{
			"single test",
			internalTestData,
			map[deps.Type][]*discovery.DependencyFile{
				deps.PythonPip: {
					{FileName: "requirements.txt", DepType: deps.PythonPip, FoundPath: filepath.Join(internalTestData, "requirements.txt")},
				},
			},
			map[string]string{
				"python.json": `{
					"fileName": "requirements.txt",
					"libs": [
						{ "name": "uvicorn" },
						{ "name": "python-dotenv" },
						{ "name": "pytest 7.2.0" },
						{ "name": "fastapi 0.104.1" },
						{ "name": "detect-secrets 1.4.0" },
						{ "name": "fastapi-health 0.3.0" },
						{ "name": "tree-sitter 0.20.4" },
						{ "name": "anthropic 0.7.7" },
						{ "name": "tensorflow 2.3.1" },
						{ "name": "alabaster 0.7.16" },
						{ "name": "certifi 2023.11.17" },
						{ "name": "babel 2.14.0" }
					]
				}`,
			},
		},
		{
			"multi type test",
			internalTestData,
			map[deps.Type][]*discovery.DependencyFile{
				deps.PythonPip: {
					{FileName: "requirements.txt", DepType: deps.PythonPip, FoundPath: filepath.Join(internalTestData, "requirements.txt")},
				},
				deps.JavaScript: {
					{FileName: "package.json", DepType: deps.JavaScript, FoundPath: filepath.Join(internalTestData, "package.json")},
				},
				deps.Ruby: {
					{FileName: "Gemfile.lock", DepType: deps.Ruby, FoundPath: filepath.Join(internalTestData, "Gemfile.lock")},
				},
			},
			map[string]string{
				"python.json": `{
					"fileName": "requirements.txt",
					"libs": [
						{ "name": "uvicorn" },
						{ "name": "python-dotenv" },
						{ "name": "pytest 7.2.0" },
						{ "name": "fastapi 0.104.1" },
						{ "name": "detect-secrets 1.4.0" },
						{ "name": "fastapi-health 0.3.0" },
						{ "name": "tree-sitter 0.20.4" },
						{ "name": "anthropic 0.7.7" },
						{ "name": "tensorflow 2.3.1" },
						{ "name": "alabaster 0.7.16" },
						{ "name": "certifi 2023.11.17" },
						{ "name": "babel 2.14.0" }
					]
				}`,
				"javascript.json": `{
					"fileName": "package.json",
					"libs": [
						{ "name": "@apollo/client ^3.5.10" },
						{ "name": "@babel/core ^7.18.5" },
						{ "name": "@rails/actioncable 7.0.8" },
						{ "name": "@rails/ujs 7.0.8" },
						{ "name": "vue 2.7.15" },
						{ "name": "jest ^28.1.3" },
						{ "name": "sass ^1.69.0" }
					]
				}`,
				"ruby.json": `{
					"fileName": "Gemfile.lock",
					"libs": [
						{ "name": "attr_encrypted (~> 3.2.4)!" },
						{ "name": "awesome_print" },
						{ "name": "bcrypt (~> 3.1, >= 3.1.14)" },
						{ "name": "better_errors (~> 2.10.1)" },
						{ "name": "capybara (~> 3.39, >= 3.39.2)" },
						{ "name": "devise (~> 4.9.3)" },
						{ "name": "kaminari (~> 1.2.2)" },
						{ "name": "rails (~> 7.0.8)" }
					]
				}`,
			},
		},
	}

	app := NewApplication()
	reportsDir, err := os.MkdirTemp("", "testReports")
	if err != nil {
		t.Fatalf("Failed to create temporary reports directory: %v", err)
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			t.Errorf("Failed to remove temporary test directory: %v", err)
		}
	}(reportsDir)

	for _, tt := range tests {
		app.scanDir = tt.scanDir
		app.reportsDir = reportsDir

		t.Run(tt.name, func(t *testing.T) {
			app.createReports(tt.depsByType)

			for depType := range tt.depsByType {
				dstFile := dstFiles[depType]

				reportFile := filepath.Join(reportsDir, "libs", dstFile)
				if _, err := os.Stat(reportFile); os.IsNotExist(err) {
					t.Errorf("Report file was not created: %s", reportFile)
				}

				content, err := os.ReadFile(reportFile)
				if err != nil {
					t.Errorf("Failed to read report file: %v", err)
				}

				var actual TestReport
				if err := json.Unmarshal(content, &actual); err != nil {
					t.Errorf("Failed to unmarshal report JSON: %v", err)
				}

				var expected TestReport
				if err := json.Unmarshal([]byte(tt.expected[dstFile]), &expected); err != nil {
					t.Errorf("Failed to unmarshal expected JSON: %v", err)
				}

				require.Equal(t, version, actual.ScannerVersion)
				require.ElementsMatch(t, expected.Libs, actual.Libs)
			}
		})
	}
}

func TestFindDependencies(t *testing.T) {
	tests := []struct {
		name     string
		scanDir  string
		deps     []*discovery.DependencyFile
		expected []*discovery.DependencyFile
	}{
		{
			"single test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DirPath: internalTestData, DepType: deps.PythonPip},
			},
		},
		{
			"custom test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "dev.txt", DirPath: internalTestData, DepType: deps.PythonPip, SearchSubdirs: true},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "dev.txt", DirPath: internalTestData + "/dev", DepType: deps.PythonPip, SearchSubdirs: true},
			},
		},
		{
			"multi type test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DepType: deps.Ruby},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DirPath: internalTestData, DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DirPath: internalTestData, DepType: deps.Ruby},
			},
		},
		{
			"multi file test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DepType: deps.PythonPip, SearchSubdirs: true},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DirPath: internalTestData, DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DirPath: internalTestData + "/dev", DepType: deps.PythonPip, SearchSubdirs: true},
			},
		},
	}

	app := NewApplication()

	for _, tt := range tests {
		app.scanDir = tt.scanDir
		t.Run(tt.name, func(t *testing.T) {
			foundDeps := app.findDependencies(tt.deps)

			require.Greater(t, len(foundDeps), 0)

			for ndx, exp := range tt.expected {
				dep := foundDeps[ndx]
				require.Equal(t, dep.DepType, exp.DepType)
				require.Equal(t, dep.Found, true)
				require.Equal(t, dep.DirPath, internalTestData)
				require.Equal(t, dep.FileName, exp.FileName)
			}
		})
	}
}
